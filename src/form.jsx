import React from 'react';
import domtoimage from 'dom-to-image';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: 'Beispieltitel',
      subtitle: 'Subtitle',
      date: 'Datum und Uhrzeit',
      place: 'Ort',
      image: 'https://images.pexels.com/photos/7142969/pexels-photo-7142969.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
    };
    this.handleChange = this.handleChange.bind(this);
  /*   this.handleSubmit = this.handleSubmit.bind(this); */
  }
  handleChange(event) {
    const value = event.target.value;
    this.setState({ [event.target.name]: value })
  }

/*   handleSubmit(event) {
    alert('Title: ' + this.state.title
      + 'SubTitle: ' + this.state.subtitle
      + 'Date: ' + this.state.date
      + 'Place: ' + this.state.place
      + 'Place: ' + this.state.image);
    event.preventDefault();
  } */

  download() {
    domtoimage.toJpeg(document.getElementById('finalsujet'), { quality: 0.95 })
      .then(function (dataUrl) {
        let link = document.createElement('a');
        link.download = 'sujet.jpeg';
        link.href = dataUrl;
        link.click();
      });
  }

  render() {
    return (
      <div className='formwrapper'>
        <div className='sujetcontainer'>
        <form /* onSubmit={this.handleSubmit} */>
          <div className='inputform'>
            <label>Title</label>
            <input type='text' value={this.state.title} onChange={this.handleChange} name='title' required></input>
          </div>
          <div className='inputform'>
            <label>Subtitle</label>
            <input type='text' value={this.state.subtitle} onChange={this.handleChange} name='subtitle' required></input>
          </div>
          <div className='inputform'>
            <label>Date and Time</label>
            <input type='text' value={this.state.date} onChange={this.handleChange} name='date' required></input>
          </div>
          <div className='inputform'>
            <label>Place</label>
            <input type='text' value={this.state.place} onChange={this.handleChange} name='place' required></input>
          </div>
          <div className='inputform'>
            <label>Image Url</label>
            <input type='text' value={this.state.image} onChange={this.handleChange} name='image' required></input>
          </div>
        </form>

        <div id='finalsujet' className='view'>
          <div className='sujet_img' style={{
            backgroundImage: `url(${this.state.image})`
          }}>
            <div className='layer'></div>
            <div className='viewinner'>
              <div className='title'>{this.state.title}</div>
              <div className='subtitle'>{this.state.subtitle}</div>
              <div className='date'>{this.state.date}</div>
              <div className='place'>{this.state.place}</div>
            </div>
          </div>
        </div>
        </div>
        <button onClick={this.download} className='downloadbtn'>Download Sujet</button>
      </div>
    );
  }
};
export default Form



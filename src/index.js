import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Form from './form';


class Sujet extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <h1>Generate your Sujet</h1>
        <div><Form /></div>
      </div>
    );
  }
};

/* --------------- */

ReactDOM.render(
  <React.StrictMode>
    <Sujet />
  </React.StrictMode>,
  document.getElementById('root')
);
